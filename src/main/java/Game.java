
import java.util.Scanner;

public class Game {

    Player playerX, playerO, turn;
    Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showBye() {
        System.out.println("Bye bye!");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        //Normal flow
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }

    public void showTurn() {
        System.out.println(table.getcurrentPlayer().getName() + " turn");
    }

    public void newGame() {
        table = new Table(playerX, playerO);
        System.out.println("Start a New Game");
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                //Show table before the game end
                this.showTable();

                if (table.getWinner() == null) {
                    System.out.println("Draw!!!");
                } else {
                    System.out.println(table.getWinner().getName() + " Win!!");
                }
                this.showBye();
                break;
                //newGame();
            }
            table.switchPlayer();
        }
    }
}
