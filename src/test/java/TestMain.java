import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class TestMain {

    public TestMain() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    public void testColum1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    public void testColum2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    public void testColum3ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    public void testTayeng1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    public void testTayeng2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    public void testRow1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    public void testRow2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    public void testRow3ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    public void testColum1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    public void testColum2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    public void testColum3ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    public void testTayeng1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    public void testTayeng2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
     public void testRow1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
     public void testRow2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
     public void testRow3ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

}